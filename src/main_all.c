//-----------------------------------------------------------------------------
// \file    main.c
// \brief   implementation of main() to test bsl drivers.
//
//-----------------------------------------------------------------------------
#include "types.h"
#include "stdio.h"

#include <stdio.h>
#include <string.h>

//-----------------------------------------------------------------------------
// \brief   entry point for bsl test code.
//
// \param   none.
//
// \return  none.
//-----------------------------------------------------------------------------
void printme( int *s, int length)
{
	int i = 0;
	printf("My output is : ");
	for( i = 0; i < length; i ++ ) {
		printf( "%d,", *(s + i) );
	}
	printf( "\n" );
}

// 冒泡排序法
// 传入待排序的数组的首地址，length为排序的长度
// 输出后ss为排序好的数组
void sub_sorts( int *ss, int length )
{
	int i,j,t = 0;
	for( i = 0; i < length; i++ ) {
		t = *(ss + i);
		for( j = i; j < length; j ++  ) {
			if( *(ss + i) >= *( ss + j ) ) {
				t = *(ss + i);
				*(ss + i) = *( ss + j );
				*(ss + j) = t;
			}
		}
	}
}


void sort_1( void )
{
	int s[16] = {5,6,4,9,1,3,8,0,7,11,12,10,14,2,19,13};
	// i 是循环因子， j为新坐标
	int i,j,t = 0;
	int len = sizeof(s) / sizeof(s[0]);

	sub_sorts(s,len);


	printf("The my output is :");
	for( i = 0; i < len; i++ ) {
		printf("%d,",s[i]);
	}
}

void divided_conquer( void )
{
	int s[16] = {5,6,4,9,1,3,8,0,7,11,12,10,14,2,19,13};
	int i,j;
	int len = sizeof(s) / sizeof(s[0]);
	int dec = len;
	while( dec > 1 ) {

		dec /= 2;
		for( i = 2; i < len + 2; i += len/dec  ) {
			sub_sorts( ( s + i - 2 ),  len/dec );
		}
		printme(s, len);

	}

	printf("The my output is :");
	for( i = 0; i < len; i++ ) {
		printf("%d,",s[i]);
	}

}


void main( void )
{
	divided_conquer();
}
